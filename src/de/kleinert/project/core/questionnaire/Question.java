package de.kleinert.project.core.questionnaire;

import java.util.ArrayList;
import java.util.List;

public class Question {

	public enum QuestionType { checkbox, radioButton, textBox }
	
	private String questionText;

	private List<String> possibleAnswers; // TextBox -> length is 0 or 1
	private List<String> chosenAnswers; // TextBox || RadioButton -> only one
										// index
	private QuestionType type;

	// if type is TextBox and possibleAnswers.get(0) is
	// not empty, it is the standard value for the textBox
	public Question(String questionText, List<String> possibleAnswers, QuestionType type) {
		this.questionText = questionText;
		this.possibleAnswers = possibleAnswers;
		this.chosenAnswers = new ArrayList<>(possibleAnswers.size());
		this.type = type;
	}
	
	// this one assumes that the type is TextBox
	public Question(String questionText){
		this(	questionText,
				new ArrayList<>(0), // possibleAnswers; no elements needed
				QuestionType.textBox);
		chosenAnswers = new ArrayList<>(1); // chosenAnswers; only one "answer" is permitted
	}

	public void answerChosen(int index) {
		// this of cause assumes that the answer exists
//		if (possibleAnswers.size() <= index)
//			throw new IndexOutOfBoundsException();
//		
//		if (type == QuestionType.radioButton)
//			chosenAnswers.clear();
		
		chosenAnswers.set(index, possibleAnswers.get(index));
	}
	
	public void setAnswer(int index, String answer){
		chosenAnswers.set(index, answer);
	}
	
	// TODO find a better name
	// only if type is TextBox
	public void textBoxInput(String value) {
		chosenAnswers.set(0, value); 
	}

	public List<String> getPossibleAnswers() { return possibleAnswers; }

	public List<String> getChosenAnswers() { return chosenAnswers; }

	public QuestionType getType() { return type; }
	
	public String getText() { return questionText; }
}
