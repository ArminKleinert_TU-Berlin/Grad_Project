package de.kleinert.project.core.questionnaire;

import java.util.ArrayList;
import java.util.List;

import de.kleinert.project.core.Task;

public class Task_Questionnaire extends Task {

	private List<Question> questions;

	public Task_Questionnaire(String name, String path, List<Question> questions) {
		super(name, TaskType.Questionnaire, path);

		if (questions == null)
			throw new IllegalArgumentException("questions list must not be null");
		this.questions = questions;
	}

	public Task_Questionnaire(String name, String path) {
		this(name, path, new ArrayList<>());
	}

	// methods for the questions-List
	public void add(Question question) {
		questions.add(question);
	}

	public void remove(int i) {
		questions.remove(i);
	}

	public void remove(Question question) {
		questions.remove(question);
	}

	public List<Question> getQuestions() {
		return questions;
	}
}
