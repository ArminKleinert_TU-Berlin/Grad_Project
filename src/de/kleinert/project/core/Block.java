package de.kleinert.project.core;

import java.util.ArrayList;
import java.util.List;

public class Block {

	private String name;
	private List<Task> tasks;
	
	public Block(String name){
		this.name = name;
		tasks = new ArrayList<Task>();
	}

	public boolean addTask(Task task) {
		return tasks.add(task);
	}
	
	public String getName() { return name; }
	public List<Task> getTasks() { return tasks; }
}
