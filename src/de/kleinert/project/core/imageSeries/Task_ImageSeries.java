package de.kleinert.project.core.imageSeries;

import java.util.List;

import de.kleinert.project.core.Task;

public class Task_ImageSeries extends Task {

	private List<String> directoryPaths;

	public Task_ImageSeries(String name, String path) {
		super(name, TaskType.Images, path);
		initializeImagePaths(path);
	}
	
	private void initializeImagePaths(String path) {
		
	}
	
	public List<String> getDirectoryPaths() {
		return directoryPaths;
	}
}
