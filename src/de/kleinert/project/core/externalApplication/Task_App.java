package de.kleinert.project.core.externalApplication;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.kleinert.project.core.Task;

public class Task_App extends Task {

	private List<String> argumentsBefore;
	private List<String> argumentsAfter;

	public Task_App(String name, String path, List<String> argumentsBefore, List<String> argumentsAfter) {
		super(name, TaskType.ExternalApp, path);

		this.argumentsBefore = argumentsBefore;
		this.argumentsAfter = argumentsAfter;
	}

	public void run() throws IOException {
		if (path == null)
			throw new NullPointerException("The file specified by path must not be null!");

		List<String> arguments = new ArrayList<String>();
		if (argumentsBefore != null)
			arguments.addAll(argumentsBefore);

		arguments.add(0, path);

		if (argumentsAfter != null)
			arguments.addAll(argumentsAfter);

		Runtime.getRuntime().exec((String[]) arguments.toArray());
	}

	public List<String> getArgumentsBefore() {
		return argumentsBefore;
	}
	
	public List<String> getArgumentsAfter() {
		return argumentsAfter;
	}

	public void setArgumentsBefore(List<String> argumentsBefore) {
		this.argumentsBefore = argumentsBefore;
	}

	public void setArgumentsAfter(List<String> argumentsAfter) {
		this.argumentsAfter = argumentsAfter;
	}
}
