package de.kleinert.project.core;
import java.util.ArrayList;
import java.util.List;

public class Experiment {

	private String name;
	private List<Block> blocks;
	
	public Experiment(String name){
		this.name = name;
		blocks = new ArrayList<Block>();
	}

	public boolean addBlock(Block block) {
		return blocks.add(block);
	}
	
	public String getName() { return name; }
	public List<Block> getBlocks() { return blocks; }
}
