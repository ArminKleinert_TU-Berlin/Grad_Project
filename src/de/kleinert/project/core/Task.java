package de.kleinert.project.core;

public abstract class Task {
	
	public static enum TaskType { Images, Questionnaire, ExternalApp }

	protected String name;
	protected TaskType type;
	protected boolean finished;
	protected String path;
	
	public Task(String name, TaskType type, String path) {
		this.name = name;
		this.type = type;
		this.path = path;
		finished = false;
	}
	
	public String getName() { return name; }
	public TaskType getType() { return type; }
	public String getPath() { return path; }
	
	// methods for the finished-variable
	public void setFinished(boolean value) { finished = value; }
	public boolean isFinished() { return finished; }
}
