package de.kleinert.project.util;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import de.kleinert.project.core.Experiment;

public class XmlExperimentReaderWriter {

	/*
	 * TODO test
	 */
	public static void writeExperimertToXml(Experiment experiment) {
		Document document = DocumentHelper.createDocument();
		Element root = document.addElement("experiment");
		root.addAttribute("name", experiment.getName());

		experiment.getBlocks().forEach(block -> {

			Element blockElement = root.addElement("block");
			blockElement.addAttribute("name", block.getName());

			block.getTasks().forEach(task -> {

				Element taskElement = blockElement.addElement("task");
				taskElement.addAttribute("name", task.getName());
				taskElement.addAttribute("type", task.getType().toString());
			});
		});
	}
}
