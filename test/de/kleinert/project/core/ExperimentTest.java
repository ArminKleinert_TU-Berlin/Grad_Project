package de.kleinert.project.core;

import org.junit.Test;

import de.kleinert.project.core.Experiment;

public class ExperimentTest {

	@Test
	public void experimentConstructorNameTest() {
		Experiment e = new Experiment("testExperiment");
		assert (e.getName() == "testExperiment");
	}

	@Test
	public void experimentConstructorBlockListEmptyTest() {
		Experiment e = new Experiment("testExperiment");
		assert (e.getBlocks() != null && e.getBlocks().isEmpty());
	}
}
