package de.kleinert.project.core;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import de.kleinert.project.core.Task;
import de.kleinert.project.core.Task.TaskType;
import de.kleinert.project.core.externalApplication.Task_App;
import de.kleinert.project.core.imageSeries.Task_ImageSeries;
import de.kleinert.project.core.questionnaire.Question;
import de.kleinert.project.core.questionnaire.Task_Questionnaire;

public class TaskTest {

	@Test
	public void taskAppConstructorNameTest() {
		Task t = new Task_App("testTask", "C://smth.exe", null, null);
		assert (t.getName() == "testTask");
	}

	@Test
	public void taskAppConstructorPathTest() {
		Task t = new Task_App("testTask", "C://smth.exe", null, null);
		assert (t.getPath() == "C://smth");
	}

	@Test
	public void taskAppConstructorTypeTest() {
		Task t = new Task_App("testTask", "C://smth.exe", null, null);
		assert (t.getType() == TaskType.ExternalApp);
	}

	@Test
	public void taskQuestionnaireConstructorNameTest() {
		Task t = new Task_Questionnaire("testTask", "C://smth");
		assert (t.getName() == "testTask");
	}

	@Test
	public void taskQuestionnaireConstructorPathTest() {
		Task t = new Task_Questionnaire("testTask", "C://smth");
		assert (t.getPath() == "C://smth");
	}

	@Test
	public void taskQuestionnaireConstructorTypeTest() {
		Task t = new Task_Questionnaire("testTask", "C://smth");
		assert (t.getType() == TaskType.Questionnaire);
	}

	@Test
	public void taskQuestionnaireConstructorQuestionListTest() {
		Task_Questionnaire t = new Task_Questionnaire("testTask", "C://smth");
		assert (t.getQuestions() != null); // if list is null, it is
											// automatically set
	}

	@Test
	public void taskQuestionnaireConstructorQuestionListTest2() {
		List<Question> l = new ArrayList<>();
		Task_Questionnaire t = new Task_Questionnaire("testTask", "C://smth", l);
		assert (t.getQuestions() == l);
	}

	@Test
	public void taskQuestionnaireConstructorQuestionListTest3() {
		List<Question> l = new ArrayList<>();
		l.add(new Question(""));
		l.add(new Question(""));
		l.add(new Question(""));
		Task_Questionnaire t = new Task_Questionnaire("testTask", "C://smth", l);
		assert (t.getQuestions().size() == 3);
	}

	@Test
	public void taskQuestionnaireConstructorQuestionListIncludeTest() {
		Question q = new Question("");
		List<Question> l = new ArrayList<>();
		Task_Questionnaire t = new Task_Questionnaire("testTask", "C://smth", l);
		assert (t.getQuestions().contains(q));
	}

	@Test
	public void taskQuestionnaireConstructorQuestionListAddTest() {
		Task_Questionnaire t = new Task_Questionnaire("testTask", "C://smth");
		t.add(new Question(""));
		t.add(new Question(""));
		t.add(new Question(""));
		assert (t.getQuestions().size() == 3);
	}

	@Test
	public void taskQuestionnaireConstructorQuestionListAddIncludeTest() {
		Question q = new Question("");
		List<Question> l = new ArrayList<>();
		Task_Questionnaire t = new Task_Questionnaire("testTask", "C://smth", l);
		t.add(q);
		assert (t.getQuestions().contains(q));
	}

	@Test
	public void taskQuestionnaireConstructorQuestionListRemoveIndexTest() {
		List<Question> l = new ArrayList<>();
		l.add(new Question(""));
		l.add(new Question(""));
		Task_Questionnaire t = new Task_Questionnaire("testTask", "C://smth", l);
		assert (t.getQuestions().size() == 2);
		t.remove(0);
		assert (t.getQuestions().size() == 1);
	}

	@Test
	public void taskQuestionnaireConstructorQuestionListRemoveIndexTest2() {
		Question q = new Question("ttt");
		List<Question> l = new ArrayList<>();
		l.add(q);
		Task_Questionnaire t = new Task_Questionnaire("testTask", "C://smth", l);
		assert (t.getQuestions().contains(q));
		t.remove(0);
		assert (t.getQuestions().size() == 0 && t.getQuestions().contains(q));
	}

	@Test
	public void taskQuestionnaireConstructorQuestionListRemoveQuestionTest() {
		Question q = new Question("ttt");
		List<Question> l = new ArrayList<>();
		l.add(q);
		Task_Questionnaire t = new Task_Questionnaire("testTask", "C://smth", l);
		assert (t.getQuestions().contains(q));
		t.remove(q);
		assert (!t.getQuestions().contains(q));
	}

	@Test
	public void taskImageSeriesConstructorNameTest() {
		Task_ImageSeries t = new Task_ImageSeries("testTask", "C://smth");
		assert (t.getName() == "testTask");
	}

	@Test
	public void taskImageSeriesConstructorPathTest() {
		Task_ImageSeries t = new Task_ImageSeries("testTask", "C://smth");
		assert (t.getPath() == "C://smth");
	}

	@Test
	public void taskImageSeriesConstructorTypeTest() {
		Task_ImageSeries t = new Task_ImageSeries("testTask", "C://smth");
		assert (t.getType() == TaskType.Images);
	}
}
