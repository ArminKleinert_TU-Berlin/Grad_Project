package de.kleinert.project.core;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import de.kleinert.project.core.questionnaire.Question;
import de.kleinert.project.core.questionnaire.Question.QuestionType;

public class QuestionTest {

	@Test
	public void questionTextTest() {
		Question q = new Question("test");
		assert (q.getText() == "test");
	}

	@Test
	public void questionTypeWhenOnlyTextTest() {
		Question q = new Question("test");
		assert (q.getType() == QuestionType.textBox);
	}

	@Test
	public void questionPossibleAnswersSizeWhenOnlyTextTest() {
		Question q = new Question("test");
		assert (q.getPossibleAnswers().size() == 0);
	}

	@Test
	public void questionPossibleAnswersTest() {
		List<String> l = new ArrayList<>();
		l.add("a0");
		Question q = new Question("test", l, QuestionType.checkbox);
		assert (q.getChosenAnswers().size() == 0);
		assert (q.getPossibleAnswers().size() == 1);
	}

	@Test
	public void questionPossibleAnswersTest2() {
		List<String> l = new ArrayList<>();
		l.add("a0");
		Question q = new Question("test", l, QuestionType.checkbox);
		assert (q.getPossibleAnswers().get(0) == "a0");
	}

	@Test
	public void questionChooseAnswerTest() {
		List<String> l = new ArrayList<>();
		l.add("a0");
		assert(l.size() == 1);
		assert (l.get(0) != null);
		Question q = new Question("test", l, QuestionType.checkbox);
		q.answerChosen(0);
		assert (q.getChosenAnswers().get(0) == l.get(0));
	}

	/*
	 * Add more tests
	 */
}
