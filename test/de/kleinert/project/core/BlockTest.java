package de.kleinert.project.core;

import org.junit.Test;

import de.kleinert.project.core.Block;

public class BlockTest {

	@Test
	public void blockConstructorNameTest() {
		Block b = new Block("testBlock");
		assert (b.getName() == "testBlock");
	}

	@Test
	public void blockConstructorTaskListEmptyTest() {
		Block b = new Block("testBlock");
		assert (b.getTasks() != null && b.getTasks().isEmpty());
	}
}
